package application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] vehicles = new Bicycle[4];
        vehicles[0] = new Bicycle("Toyota", 30, 250.00);
        vehicles[1] = new Bicycle("Porshe", 12, 150.00);
        vehicles[2] = new Bicycle("Honda", 5, 50.00);
        vehicles[3] = new Bicycle("lamborgini", 100, 400.00);

        for (int i = 0; i < vehicles.length; i++){
            System.out.println(vehicles[i]);
        }
    }
}
